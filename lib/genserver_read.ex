defmodule TrabalhoAed do
  @moduledoc """
    GenServer responsável por iniciar as buscas na base de dados e persistir em tempo de execução os dados
  """

  use GenServer
  require Logger

  alias TrabalhoAed.OrderFunctions
  alias TrabalhoAed.Binary

  def start_link() do
    GenServer.start_link(__MODULE__, %{}, name: :persistent_process)
  end

  def init(data) do

      state = Map.merge(%{data_return: []}, data)
      {:ok, state, {:continue, :module_opening}}
  end


  def handle_continue(:module_opening, state) do

    [time: _, humanized_duration: humanized_duration, reply: state] =
      Timer.tc(fn ->
        desorder_data = get_data()
        data_return = order_data(desorder_data )

        Map.merge(state, %{data_return: data_return, desorder_data: desorder_data})
      end)

      IO.inspect(humanized_duration, label: "order")



    {:noreply, state}
  end

  def handle_cast(:order_reverse, %{data_return: data_return} = state) do
    desorder_data = Enum.reverse(data_return)

    [time: _, humanized_duration: humanized_duration, reply: state] =
    Timer.tc(fn ->
      data_reverse = order_data(desorder_data )

       Map.merge(state, %{data_reverse: data_reverse})
    end)

    IO.inspect(humanized_duration, label: "order_reverse")

    {:noreply, state}
  end

  def handle_cast(:order_order, %{data_return: data_return} = state) do

    [time: _, humanized_duration: humanized_duration, reply: state] =
    Timer.tc(fn ->

      data_return = order_data(data_return)

       Map.merge(state, %{data_return: data_return})
    end)


    IO.inspect(humanized_duration, label: "order_order")

    {:noreply, state}
  end




  def handle_call({:search_range, value, key}, _from,  state) do

    [time: _, humanized_duration: humanized_duration, reply: values] =
      Timer.tc(fn ->

        case key do
        :order ->
          Binary.search_binary(state.data_return, "hhcode", value)
        :natural ->
          Binary.search_binary(state.desorder_data, "hhcode", value)
        :reverse ->
          Binary.search_binary(state.data_reverse, "hhcode", value)
        end
      end)

    IO.inspect(humanized_duration, label: "time for request")

    {:reply, values, state}
  end

  def get_data() do
    "../data.csv"
    |> Path.expand(__DIR__)
    |> File.stream!
    |> CSV.decode!(headers: true)
    |> Enum.take(9598)
    |> Enum.shuffle()
  end

  def order_data(data) do
    data
    |> Enum.map(&( Map.put(&1, "nyieldc1", transform(&1["nyieldc1"]) ) ) )
    |> OrderFunctions.qsort()
  end


  def finded_safra(hhcode, :order) do
    GenServer.call(:persistent_process, {:search_range, hhcode, :order }, 180_000)
  end

  def finded_safra(hhcode, :natural) do
    GenServer.call(:persistent_process, {:search_range, hhcode, :natural}, 180_000)
  end

  def finded_safra(hhcode, :reverse) do
    GenServer.call(:persistent_process, {:search_range, hhcode, :reverse}, 180_000)
  end

  defp transform(""), do: 0

  defp transform(value) do
    if is_integer(value) do
      value
    else
      value
      |> String.to_float()
      |> round()
    end
  end

end
